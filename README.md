## CLFLUSH instructions after StoreInst instructions ##

This pass stops particular values of a program from ever being cached on the processor. It can be used in two obvious ways, first, to find the data that most heavily relies on caching, and second, to find the data which may lead to false capacity evictions. Both ways are based around the CLFLUSH instruction, and works as follows:

1. An integer X is read from the file exemptList.txt 
2. The Xth StoreInst instruction in the program is found
3. A CLFLUSH instruction is added at the end of that BasicBlock
4. The performance of the new program is measured using: perf stat -e cycles,instructions,cache-misses 
5. This process is repeated for every StoreInst in the program. Either incrementally adding a CLFLUSH after every StoreInst, or adding CLFLUSH's to every StoreInst and incrementally removing them.

**Top-down: To find the data that relies on caching the most, add CLFLUSH instructions**
Start with no CLFLUSH instructions, then modify one StoreInst at a time, and save the version which results in the *most* cache misses. This will find the ordered set of StoreInst instructions that are most heavily cached. Or more precisely, the set of StoreInst that, if not cached, would lead to the worst performance.

.
.
<Insert Picture>
.
.

**Bottom-up: To find any values that lead to false capacity cache evictions, saturate then remove CLFLUSH instructions**
Start with every StoreInst modified with a CLFLUSH instruction, then remove one CLFLUSH instruction at a time, and save the result which results in the *least* cache misses. 

For a simple array search program (see experiment1.cpp), we get the following performance change after each of the 11 StoreInst/CLFLUSH pairs are removed.

![Experiment1.png](https://bitbucket.org/repo/z8jgM4g/images/4171722661-Experiment1.png)

**Pre-fetch