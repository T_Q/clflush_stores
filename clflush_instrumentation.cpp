#include <cstdint>
#include <cstdio>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

using namespace std;
extern "C" {

  uint64_t flushes = 0;
  //Clear Cache line at address
  void clflush(void *p)
  {
    asm volatile ("clflush (%0)" :: "r"(p));
    flushes++;
  }

  void print_clflushes() {
    printf("Number of CLFlushes Executed: %ld\n",flushes);
  }
  //Read Time-Stamp Counter
  inline uint64_t
  rdtsc()
  {
    unsigned long a, d;
    asm volatile ("cpuid; rdtsc" : "=a" (a), "=d" (d) : : "ebx", "ecx");
    return a | ((uint64_t)d << 32);
  }

}
