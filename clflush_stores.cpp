#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/UnrollLoop.h"
#include "llvm/Analysis/CFGPrinter.h"
#include "llvm/Support/FileSystem.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/Transforms/IPO/InlinerPass.h"
#include "llvm/Transforms/IPO.h"
#include <unordered_map>
#include <utility>

using namespace std;
using namespace llvm;


struct clflush_stores : public ModulePass {
  static char ID;

  clflush_stores() : ModulePass(ID) {}
  virtual ~clflush_stores() { }


  virtual void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesAll();
  }

  virtual bool runOnModule(Module &M) override;
};


static const char h_name[] = "clflush_stores Pass";
char clflush_stores::ID = 0;
static RegisterPass<clflush_stores> X("clflush_stores", "voidTy Pass");


Pass *createCLFLUSH_STORESPass() {
  return new clflush_stores();
}

//Insert clflush after loc instruction
Instruction* clflush(Instruction *loc)
{
  assert(isa<StoreInst>(loc) && "Insert location is not a store instruction");

  //The address we wish to flush (the operand of a storeInst)
  Value *address = loc->getOperand(1);

  BasicBlock *parent_block = loc->getParent();
  Function *parent_function = parent_block->getParent();
  Module *parent_module = parent_function->getParent();
  LLVMContext &context= parent_function->getContext();

  //Create the function definition, and add it to the module
  auto *voidTy = Type::getVoidTy(context); //the return type
  auto *voidPtrTy = Type::getInt8PtrTy(context); //the address type
  vector<Type*> params;
  params.push_back(voidPtrTy);
  auto *helperTy = FunctionType::get(voidTy, params, false);
  auto *clflush_fun = parent_module->getOrInsertFunction("clflush", helperTy);

  //Create the function call, and add it after the loc instruction
  IRBuilder<> Builder(parent_block->getTerminator());

  vector<Value*>args;
  args.push_back(Builder.CreatePointerCast(address, voidPtrTy));
  Instruction *clflush_call = Builder.CreateCall(clflush_fun, args);

  return clflush_call;
}

//Insert call to print the number of flushes done
Instruction* print_clflushes(Instruction *loc)
{

  BasicBlock *parent_block = loc->getParent();
  Function *parent_function = parent_block->getParent();
  Module *parent_module = parent_function->getParent();
  LLVMContext &context= parent_function->getContext();

  //Create the function definition, and add it to the module
  auto *voidTy = Type::getVoidTy(context); //the return type
  vector<Type*> params;
  auto *helperTy = FunctionType::get(voidTy, params, false);
  auto *clflush_fun = parent_module->getOrInsertFunction("print_clflushes", helperTy);

  //Create the function call, and add it after the loc instruction
  IRBuilder<> Builder(parent_block->getTerminator());

  vector<Value*>args;
  Instruction *clflush_call = Builder.CreateCall(clflush_fun, args);

  return clflush_call;
}


bool clflush_stores::runOnModule(Module &M)
{

  vector<StoreInst*> storeInsts;

  int count = 1;
  vector<int> exempt;
  ifstream myfile("exemptList.txt");
  string line;
  while (std::getline(myfile, line)) {
    exempt.push_back(atoi(line.c_str()));
  }

  //Read in exempt vector from file

  for (Function &F : M) {
    for (BasicBlock &B : F) {
      if (F.getName().find("main") == string::npos) continue;

      for (Instruction &I : B) {
        if (isa<StoreInst>(I)) {
          if (find(exempt.begin(),exempt.end(),count) == exempt.end()) {
            clflush(&I);
            // outs() << "Adding flush " << count << "\n";
          } else {
            // outs() << "Skipping flush " << count << "\n";
          }
          count++;
        }
        if (isa<ReturnInst>(I)) {
          // print_clflushes(&I);
        }
      }
    }
  }

  outs() << count << "\n";
  return true;
}
