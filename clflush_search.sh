#!/bin/bash

SOURCE=$1
INPUT=$2
echo "" > exemptList.txt
echo "" > exemptLast.tmp


#Compiling baseline
clang++ $SOURCE -o $SOURCE.baseline.bin

#Run the baseline
perf stat -e cycles,instructions,cache-misses -x, ./$SOURCE.baseline.bin $INPUT 2> baseline_stats.csv 1> /dev/null

#Gathering baseline statistics
baseline_instructions=0
baseline_cycles=0;
baseline_cache_misses=0;
IFS=,dd
while read count blank name ref time
do
    if [ $name == "instructions" ]; then
        baseline_instructions=$count
    fi
    if [ $name == "cycles" ]; then
        baseline_cycles=$count
    fi
    if [ $name == "cache-misses" ]; then
        baseline_cache_misses=$count
    fi
done < baseline_stats.csv

#Add CLFLUSH instruction after every StoreInst, record statistics
./run_experiment.sh "$@"
NUM_CLFLUSH=`cat OptimizationOut.txt`
echo "CLFLUSH's added: $NUM_CLFLUSH"
IFS=', ' read -r -a array <<< `cat RunOutput.csv`

echo "Index,Misses,Cycles,Instructions"
echo "Baseline,$baseline_cache_misses,$baseline_cycles,$baseline_instructions"
echo "FullyFlushed,${array[0]},${array[1]},${array[2]}"

#Incrementally remove each CLFLUSH instruction, record performance changes, and
#permanently remove the CLFLUSH instruction that caused the most cache misses
lowestIdx=0;
lowestMiss=${array[0]}
currentCycles=${array[1]};
currentInst=${array[2]};

echo "" > exemptLast.tmp
for (( x = 0; x <=${NUM_CLFLUSH}; x++ )); do
    for (( i = 0; i <=${NUM_CLFLUSH}; i++ )); do

        #Check to see if i is already exempted
        if grep -Fxq "$i" exemptLast.tmp; then
            continue;
        fi

        #Add this index temporarily to the exempt list
        cp exemptLast.tmp exemptList.txt
        echo $i >> exemptList.txt

        #Run the experiment
        ./run_experiment.sh "$@"

        #Get statistics, and check for improvement
        IFS=', ' read -r -a array <<< `cat RunOutput.csv`
        misses=${array[0]}
        if [ "$misses" -lt "$lowestMiss" ]; then
            lowestMiss=$misses
            lowestIdx=$i
            currentCycles=${array[1]}
            currentInst=${array[2]}
        fi
    done

    #Select the index which has the fewest cache misses
    echo $lowestIdx >> exemptLast.tmp

    #Show progress
    echo "$lowestIdx,$lowestMiss,$currentCycles,$currentInst"
done

#Remove temporary files
rm exemptLast.tmp RunOutput.csv OptimizationOut.txt
rm $SOURCE.baseline.bin
rm baseline_stats.csv
