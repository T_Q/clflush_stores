#!/bin/bash

SOURCE=$1
INPUT=$2

#echo "Applying optimization"
opt -load clflush_stores.so -clflush_stores $SOURCE -o $SOURCE.applied.bc > OptimizationOut.txt

#echo "Compiling optimized version"
clang++ -std=c++11 $SOURCE.applied.bc clflush_instrumentation.cpp -o $SOURCE.applied.bin

#echo "Running optimized"
perf stat -e cycles,instructions,cache-misses -x, ./$SOURCE.applied.bin $INPUT 2> flushed_stats.csv 1> /dev/null

#Get statistics on clflushed version
flushed_instructions=0
flushed_cycles=0;
flushed_cache_misses=0;
IFS=,dd
while read count blank name ref time
do
    if [ $name == "instructions" ]; then
        flushed_instructions=$count
    fi
    if [ $name == "cycles" ]; then
        flushed_cycles=$count
    fi
    if [ $name == "cache-misses" ]; then
        flushed_cache_misses=$count
    fi
done < flushed_stats.csv

echo $flushed_cache_misses,$flushed_cycles,$flushed_instructions > RunOutput.csv

rm flushed_stats.csv
rm $SOURCE.applied.bc $SOURCE.applied.bin

exit















































#Some possibly useful stuff:
echo "Compiling baseline"
clang++ $SOURCE.ll -o $SOURCE.baseline.bin

echo "Running baseline"
perf stat -e cycles,instructions,cache-misses -x, ./$SOURCE.baseline.bin $INPUT 2> baseline_stats.csv


baseline_instructions=0
baseline_cycles=0;
baseline_cache_misses=0;
IFS=,dd
while read count blank name ref time
do
    if [ $name == "instructions" ]; then
        baseline_instructions=$count
    fi
    if [ $name == "cycles" ]; then
        baseline_cycles=$count
    fi
    if [ $name == "cache-misses" ]; then
        baseline_cache_misses=$count
    fi
done < baseline_stats.csv

instruction_d=`expr $flushed_instructions - $baseline_instructions`
instruction_r=`bc -l <<< "(($instruction_d / $baseline_instructions * 100))"`
cycle_d=`expr $flushed_cycles - $baseline_cycles`
cycle_r=`bc -l <<< "(($cycle_d / $baseline_cycles * 100))"`
cache_d=`expr $flushed_cache_misses - $baseline_cache_misses`
cache_r=`bc -l <<< "(($cache_d / $baseline_cache_misses *100))"`



printf "Flushed had %.2f%% (%d) more instructions\n" $instruction_r $instruction_d
printf "Flushed had %.2f%% (%d) more cycles\n" $cycle_r $cycle_d
printf "Flushed had %.2f%% (%d) more cache misses\n" $cache_r $cache_d

exit
echo "Baseline Instructions: $baseline_instructions"
echo "Baseline Cycles: $baseline_cycles"
echo "Baseline Cache Misses: $baseline_cache_misses"
echo "Flushed Instructions: $flushed_instructions"
echo "Flushed Cycles: $flushed_cycles"
echo "Flushed Cache Misses: $flushed_cache_misses"
echo "Delta from baseline: Flushed-Baseline (+Percent Change%)"
echo "Instruction delta: $instruction_d ($instruction_r)"
echo "Cycle delta: $cycle_d "
echo "Cache miss delta: $cache_d"
