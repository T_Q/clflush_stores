#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define SIZE 1024 //Size of int array to fill
#define TESTS 1024 //Number of times to repeat experiment
//Read Time-Stamp Counter
inline uint64_t
rdtsc()
{
    unsigned long a, d;
    asm volatile ("cpuid; rdtsc" : "=a" (a), "=d" (d) : : "ebx", "ecx");
    return a | ((uint64_t)d << 32);
}

inline int func(int *A) {
  int sum = 0;
  for (int i=0;i<SIZE;++i) {
    sum += A[i];
  }
  return sum;
}

inline uint64_t test(int *A)
{
    uint64_t start, end;
    start = rdtsc();
    func(A);
    end = rdtsc();
    return end-start;
}

int main(int ac, char **av)
{
  uint64_t ticks1, ticks2;
  // int test1 = 0, test2 = 0, delta = 0;

  for (int t=0;t<TESTS;++t) {

    //Load an array into cache
    int A[SIZE];
    for (int i=0;i<SIZE;++i) {
      A[i] = i;
    }

    //Run one, not cached if clflush_stores worked
    ticks1 = test(A);

    //Run two, should be cached
    ticks2 = test(A);

    // printf("Run 1: %ld ticks\n",ticks1);
    // printf("Run 2: %ld (d=%ld)\n",ticks2,ticks2-ticks1);
    // delta += (ticks2 - ticks1);
    // test1 += ticks1;
    // test2 += ticks2;
    // printf("-----------------\n");
  }

  // float avgDelta = delta / TESTS;
  // float avgTest1 = test1 / TESTS;
  // float avgTest2 = test2 / TESTS;
  // printf("Average delta over %d runs: %.1f, Avg Test1: %.1f, Avg Test2: %.1f\n", TESTS, avgDelta,avgTest1,avgTest2);
  return 0;
}
